import React, { useState, useEffect } from "react";
import {ListGroup} from "react-bootstrap";
function FetchData() {
const [count,setCount]=useState(0);
const [person,setPerson]=useState(null);

  useEffect(async () => {
    const response = await fetch("https://randomuser.me/api/?page=1&results=30")
    const data = await response.json();
    const [item] = data.results;
    setPerson(item)
  }, []);
  for(let i=1;i<=person;i++){
    FetchData()
  }
  return (
    <>
    <div>
    {person && <div>
        <ListGroup as="ul">
    <ListGroup.Item as="li" active>
    <a href={person.picture.large}>{person.picture.large}</a>
    </ListGroup.Item>
    <ListGroup.Item as="li">
    {person.name.first}
    </ListGroup.Item>
    <ListGroup.Item as="li">
    {person.name.last}
    </ListGroup.Item>
    <ListGroup.Item as="li">
    {person.email}
    </ListGroup.Item>
  </ListGroup>
      </div>}
    </div>
    <div>
    {person && <div>
        <ListGroup as="ul">
    <ListGroup.Item as="li" active>
    <a href={person.picture.large}>{person.picture.large}</a>
    </ListGroup.Item>
    <ListGroup.Item as="li">
    {person.name.first}
    </ListGroup.Item>
    <ListGroup.Item as="li">
    {person.name.last}
    </ListGroup.Item>
    <ListGroup.Item as="li">
    {person.email}
    </ListGroup.Item>
  </ListGroup>
      </div>}
    </div>
    <div>
    {person && <div>
        <ListGroup as="ul">
    <ListGroup.Item as="li" active>
    <a href={person.picture.large}>{person.picture.large}</a>
    </ListGroup.Item>
    <ListGroup.Item as="li">
    {person.name.first}
    </ListGroup.Item>
    <ListGroup.Item as="li">
    {person.name.last}
    </ListGroup.Item>
    <ListGroup.Item as="li">
    {person.email}
    </ListGroup.Item>
  </ListGroup>
      </div>}
    </div>
    </>
  );
}

export default FetchData;
